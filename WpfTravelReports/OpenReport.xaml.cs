﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace WpfTravelReports
{
    /// <summary>
    /// Interaction logic for OpenReport.xaml
    /// </summary>
    public partial class OpenReport : Window
    {
        public static TravelExpensesSystemEntities tre = new TravelExpensesSystemEntities();

        public int reportNr = 0;
        public OpenReport()
        {
            InitializeComponent();
           

        }
        private void Form_Loaded(object sender, RoutedEventArgs e)
        {
            RaportiNumberTxtBlock.Text = reportNr.ToString();         //Paneb avatud raporti numbri current reporti kõrvale




            //this.Open_Report.ItemsSource = App.tre.Expenses.ToList()                // sisestab datagridi expenses tabeli andmed
            //  .Where(x => this.reportNr == 0 || x.ExpensesID == this.reportNr)
            //  .ToList();


            this.Open_Report.ItemsSource = App.tre.Report.ToList()                   // sisestab datagridi report tabeli andmed (hetkel millegipärast sisestab ainult ühe tabelist, kui mõlemad on aktiveeritud
               .Where(x => this.reportNr == 0 || x.ReportNr == this.reportNr)         //niisama lihtsalt vist ei saagi ühendada, tuleb valida, kas ühest või teisest tabelist andmed...
               .ToList();

        }

        private void Ajalugubtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Kinnitusbtn_Click(object sender, RoutedEventArgs e)
        {



            Button a = (Button)sender;     //muudab peale klikkides nupu nime
            a.Content = "Confirmed";
            

            tre.Report.Where(x => x.ReportNr == reportNr).SingleOrDefault().Status = "Confirmed";   // muudab databaasis staatuse kinnitatuks

            tre.SaveChanges();   // salvestab muudatused


        }

        private void Lisatud_failidbtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Muudabtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.Open_Report.SelectedItem != null)
            {
                (new ChangeReport { reportNr = ((Report)(Open_Report.SelectedValue)).ReportNr }).Show();
            }
            //    Changing_report teine = new Changing_report();
            //    teine.Show();
        }
    }

}