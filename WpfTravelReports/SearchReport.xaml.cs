﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace WpfTravelReports
{
    /// <summary>
    /// Interaction logic for SearchReport.xaml
    /// </summary>
    public partial class SearchReport : Window

    {
        public static string SearchName;

        //  public static TravelExpensesSystemEntities tre = new TravelExpensesSystemEntities();

        public int reportNr = 0;
        public SearchReport()
        {
            InitializeComponent();
            DatePicker_Lopp.SelectedDate = DateTime.Today;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            //DateTime Algus = DatePicker_Algus.SelectedDate.Value;
            //DateTime Lopp = DatePicker_Lopp.SelectedDate.Value;

            SearchName = this.txtEmployID.Text;  //see on sisestatud töötaja ID

            if (DatePicker_Algus.SelectedDate == null)
            {
                MessageBox.Show("Enter Start Date");
                return;
            }
            else if (string.IsNullOrEmpty(txtEmployID.Text))
            {
                this.Dtgrd_SearchResults.ItemsSource = App.tre.Report.ToList()          // võtab kõigi raportid, sest employee lahter on tühi
                   .Where(x => x.StartDate >= DatePicker_Algus.SelectedDate.Value && x.FinishDate <= DatePicker_Lopp.SelectedDate.Value)
                   .Reverse()
                   .ToList();
            }
            else
            {
                this.Dtgrd_SearchResults.ItemsSource = App.tre.Report.ToList()
                    //       .Where(x => this.reportNr == 0 || x.ReportNr == this.reportNr)
                    .Where(x => x.EmployeeID == int.Parse(SearchName))           // võtab Useri raportid
                    .Where(x => x.StartDate >= DatePicker_Algus.SelectedDate.Value && x.FinishDate <= DatePicker_Lopp.SelectedDate.Value)
                    .Reverse()                                              // pöörab raporti nimekirja ümber,                                            
                    .ToList();
            }
        }
    }


}