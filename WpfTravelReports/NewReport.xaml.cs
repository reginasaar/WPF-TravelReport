﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Entity;
using System.Data;

namespace WpfTravelReports
{
    /// <summary>
    /// Interaction logic for NewReport.xaml
    /// </summary>
    public partial class Expenses
    {
       public static List<string> ExpensesTypes = new List<string> { "Accommodation", "Transport", "Other" };

       // public enum ExpensesTypes { SELECT, Accommodation, Transport, Other  };

    }
    public partial class Report
    {                                                                                                           // Unsubmibmitted - esitatud
        public static string[] ListStatus = new string[] { "Unsubmitted", "Submitted", "Confirmed"};            // Submitted - kinnitatud supervisori poolt
    }                                                                                                           // Confirmed - kinnitatud bookkeeperi poolt    

    public partial class NewReport : Window
    {
      //  TravelExpensesSystemEntities tre = new TravelExpensesSystemEntities();
        // public int expensesID;
       
        Report newReport = new Report();

        public NewReport()
        {
            InitializeComponent();
            
            this.txtFirtstName.Text = App.tre.Employee.ToList()
            .Where(x => x.EmployeeID == int.Parse(Login.UserName))
            .SingleOrDefault()?.FirstName;

            this.TxtLastName.Text = App.tre.Employee.ToList()
            .Where(x => x.EmployeeID == int.Parse(Login.UserName))
            .SingleOrDefault()?.SecName;

            txtEmplID.Text = Login.UserName;
            this.txtDateNow.Text = DateTime.Now.ToShortDateString();

         //   TabelExpenses.DataContext = App.tre.Expenses.ToList();
         //   TabelExpenses.ItemsSource = App.tre.Expenses.ToList();
       
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
          //  newReport.ReportNr = App.tre.Report.Count() + 1;                  // Peaks raporti nr ise andmebaasi panema, kuidas mina selle siia saan? V on seda üldse vaja?
            newReport.EmployeeID = int.Parse(Login.UserName);

            this.TabelExpenses.ItemsSource = App.tre.Expenses.Take(0).ToList();         // Tühja gridi saamiseks, millel oleks õiged parameetrid tuleb lisada Take(0)
            ComboboxCol.ItemsSource = Expenses.ExpensesTypes;
        }

        private void btnCalculateSum_Click(object sender, RoutedEventArgs e)
        {
            int tot = 0;
            int totTransport = 0;
            int totAccommodation = 0;
            int totOther = 0;

            for (int i = 0; i < TabelExpenses.Items.Count-1; i++)
            {
                // Arvutab kogu Expenses summa
                tot += (int.Parse((TabelExpenses.Columns[1]                        // Aga kui väärtus on null ....
                    .GetCellContent(TabelExpenses.Items[i]) as TextBlock).Text));

                // Arvutab Transpordi kogusumma
                if ((TabelExpenses.Columns[0].GetCellContent(TabelExpenses.Items[i]) as ComboBox).Text == "Transport")      // Kas Equals ja == on täpselt sama asi ??
                {
                    totTransport += (int.Parse((TabelExpenses.Columns[1].GetCellContent(TabelExpenses.Items[i]) as TextBlock).Text));   
                }

                // arvutab Accommodationi summa
                if ((TabelExpenses.Columns[0].GetCellContent(TabelExpenses.Items[i]) as ComboBox).Text.Equals("Accommodation"))
                {
                    totAccommodation += (int.Parse((TabelExpenses.Columns[1].GetCellContent(TabelExpenses.Items[i]) as TextBlock).Text));
                }

                //Arvutab Other summa
                if ((TabelExpenses.Columns[0].GetCellContent(TabelExpenses.Items[i]) as ComboBox).Text.Equals("Other"))
                {
                    totOther += (int.Parse((TabelExpenses.Columns[1].GetCellContent(TabelExpenses.Items[i]) as TextBlock).Text));
                }
            }
            txtTotalExpenses.Text = tot.ToString();
            txtTotalTransport.Text = totTransport.ToString();
            txtTotalAccommodation.Text = totAccommodation.ToString();
            txtTotalOther.Text = totOther.ToString();

            if (string.IsNullOrEmpty(txtDays.Text))
            {
                MessageBox.Show("Data is empty");
                return;
            }


             txtAllowance.Text = (int.Parse(txtDays.Text) * 50).ToString(); 
                                                   // Kui Days kast on tühi ja ta Message boxi ette annab kas siis salvestada saab?

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (  string.IsNullOrEmpty(txtDestination.Text)             // Kontrollib, kas antud lahtrid on nullid, ja avab siis messageboxi
               || string.IsNullOrEmpty(txtTotalExpenses.Text)            // Kas see võib tühi olla?
               || string.IsNullOrEmpty(txtDays.Text)                    // Kas erroreid peaks püüdma try catchiga ??
               || (DatePickerStart.SelectedDate == null)
               || (DatePickerFinish.SelectedDate == null)
               )
            {
                MessageBox.Show("Data is empty");
                return;
            }

            newReport.Destination = txtDestination.Text;
            newReport.StartDate = DatePickerStart.SelectedDate.Value;
            newReport.FinishDate = DatePickerFinish.SelectedDate.Value;
            newReport.TotalExpenses = int.Parse(txtTotalExpenses.Text);
            newReport.Days = int.Parse(this.txtDays.Text);
            newReport.AccommodationTotal = int.Parse(txtTotalAccommodation.Text);
            newReport.TransportTotal = int.Parse(txtTotalTransport.Text);
            newReport.OtherExpensesTotal = int.Parse(txtTotalOther.Text);

            newReport.DailyAllowance = int.Parse(txtAllowance.Text);

            App.tre.Report.Add(newReport);
            App.tre.SaveChanges();

            foreach ( var x in TabelExpenses.ItemsSource)
            {
                if (x is Expenses) newReport.Expenses.Add((Expenses)x);
            }
  
            App.tre.SaveChanges();
            Close();

        }
        private void TabelExpenses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        

    }
}
