﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfTravelReports
{
    /// <summary>
    /// Interaction logic for ChangeReport.xaml
    /// </summary>
    public partial class ChangeReport : Window
    {
        public int reportNr;
        ObservableCollection<Expenses> col;
        Report thisReport = new Report();

        public ChangeReport()
        {
            InitializeComponent();          

            this.txtFirtstName.Text = App.tre.Employee.ToList()
           .Where(x => x.EmployeeID == int.Parse(Login.UserName))
           .SingleOrDefault()?.FirstName;

            this.TxtLastName.Text = App.tre.Employee.ToList()
            .Where(x => x.EmployeeID == int.Parse(Login.UserName))
            .SingleOrDefault()?.SecName;

            txtEmplID.Text = Login.UserName;
            this.txtDateNow.Text = DateTime.Now.ToShortDateString();
            ComboboxCol.ItemsSource = Expenses.ExpensesTypes;
        }
        private void Wind_Loaded(object sender, RoutedEventArgs e)
        {

            //this.TabelExpenses.ItemsSource = App.tre.Report.ToList()
            //    .Where(x => this.reportNr == 0 || x.ReportNr == this.reportNr)
            //    .ToList();

            //Report thisReport = App.tre.Report.Where(x => x.ReportNr == reportNr).SingleOrDefault();
            thisReport = App.tre.Report.Where(x => x.ReportNr == reportNr).SingleOrDefault();

            if (thisReport != null)
            {

                txtDestination.Text = thisReport.Destination;
                txtDays.Text = thisReport.Days.ToString();
                DatePickerStart.Text = thisReport.StartDate.ToString();
                DatePickerFinish.Text = thisReport.FinishDate.ToString();

                this.TabelExpenses.ItemsSource = thisReport.Expenses.ToList();

            }
        }
        private void btnCalculateSum_Click(object sender, RoutedEventArgs e)
        {
            int tot = 0;
            int totTransport = 0;
            int totAccommodation = 0;
            int totOther = 0;

            for (int i = 0; i < TabelExpenses.Items.Count-1; i++)
            {
                // Arvutab kogu Expenses summa
                tot += (int.Parse((TabelExpenses.Columns[1]                        // Aga kui väärtus on null ....
                    .GetCellContent(TabelExpenses.Items[i]) as TextBlock).Text));

                // Arvutab Transpordi kogusumma
                if ((TabelExpenses.Columns[0].GetCellContent(TabelExpenses.Items[i]) as ComboBox).Text == "Transport")      // Kas Equals ja == on täpselt sama asi ??
                {
                    totTransport += (int.Parse((TabelExpenses.Columns[1].GetCellContent(TabelExpenses.Items[i]) as TextBlock).Text));
                }

                // arvutab Accommodationi summa
                if ((TabelExpenses.Columns[0].GetCellContent(TabelExpenses.Items[i]) as ComboBox).Text.Equals("Accommodation"))
                {
                    totAccommodation += (int.Parse((TabelExpenses.Columns[1].GetCellContent(TabelExpenses.Items[i]) as TextBlock).Text));
                }

                //Arvutab Other summa
                if ((TabelExpenses.Columns[0].GetCellContent(TabelExpenses.Items[i]) as ComboBox).Text.Equals("Other"))
                {
                    totOther += (int.Parse((TabelExpenses.Columns[1].GetCellContent(TabelExpenses.Items[i]) as TextBlock).Text));
                }
            }
            txtTotalExpenses.Text = tot.ToString();
            txtTotalTransport.Text = totTransport.ToString();
            txtTotalAccommodation.Text = totAccommodation.ToString();
            txtTotalOther.Text = totOther.ToString();

            if (string.IsNullOrEmpty(txtDays.Text))
            {
                MessageBox.Show("Data is empty");
                return;
            }

              if (txtDays.Text != "")
            { txtAllowance.Text = (int.Parse(txtDays.Text) * 50).ToString(); }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            foreach (var x in TabelExpenses.ItemsSource)
            {
                if (x is Expenses) thisReport.Expenses.Add((Expenses)x);
            }

            thisReport.DailyAllowance = int.Parse(txtAllowance.Text);

            App.tre.SaveChanges();
            Close();

        }
        private void btnDelExpens_Click(object sender, RoutedEventArgs e)
        {
            if (TabelExpenses.SelectedIndex >= 0)
            {
                //remove the selectedItem from the collection source
                Expenses thisExpens = TabelExpenses.SelectedItem as Expenses;
                col.Remove(thisExpens);
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
