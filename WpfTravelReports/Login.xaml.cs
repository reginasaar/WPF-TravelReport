﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfTravelReports
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public static string UserName;
        public Login()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)       
        {
            Close();
        }
       
        private void btnLogin_Click(object sender, RoutedEventArgs e1)      // Exeption ja siin ei saa mõlemad olla e-d, panin nr 1 lõppu
        {
            UserName = this.txtUserID.Text;

            try
            {
                //if ()
                //{ MessageBox.Show("Data is empty");
                //    return;
                //}
                (new MainWindow()).Show();
            }

            catch (Exception e)
            {
                MessageBox.Show("Exception caught.");
                return;
            }

           
        }


    }
}
