﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfTravelReports
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
 

    public partial class MainWindow : Window
    {
        TravelExpensesSystemEntities tre = new TravelExpensesSystemEntities();      // Miks seda uuesti on vaja??? App.xaml.cs-is juba on?
        public int reportNr = 0;

        public MainWindow()
        {
            InitializeComponent();

            
            this.TabelReports.ItemsSource = tre.Report.ToList();
            this.DateNow.Text = DateTime.Now.ToShortDateString();
           
            string eesNimi = App.tre.Employee.ToList()
            .Where(x => x.EmployeeID == int.Parse(Login.UserName))
            .SingleOrDefault()?.FirstName;

            string pereNimi = App.tre.Employee.ToList()
            .Where(x => x.EmployeeID == int.Parse(Login.UserName))
            .SingleOrDefault()?.SecName;

            string fullName = $"{eesNimi} {pereNimi}";
            this.txtUser.Text = fullName;
        }
        private void Window_loaded(object sender, RoutedEventArgs e)
        {
            this.TabelReports.ItemsSource = App.tre.Report.ToList()
                .Where(x => this.reportNr == 0 || x.ReportNr == this.reportNr)
                .Where(x => x.EmployeeID == int.Parse(Login.UserName))  // võtab Useri raportid
                .Reverse()                                              // pöörab raporti nimekirja ümber, 
                .Take(20)                                               // Võtab esimesed, mis viimati sisestatud, mitte kuupäeva järgi
                .ToList();
        }

        private void Rep_Selected(object sender, RoutedEventArgs e)
        {
            if (TabelReports.SelectedValue != null)
            {
                reportNr = ((Report)(TabelReports.SelectedValue)).ReportNr;
            }
        }
        private void btnOpenReport_Click(object sender, RoutedEventArgs e)
        {
            if (this.TabelReports.SelectedItem != null)
            {
                (new OpenReport { reportNr = ((Report)(TabelReports.SelectedValue)).ReportNr }).Show();
            }
        }

        private void btnNewReport_Click(object sender, RoutedEventArgs e)
        {
            (new NewReport()).Show();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            (new SearchReport()).Show();
        }
    }
    
}
